
import java.util.Scanner;

public class Programmierübung {
	
	public static double berechneMittelwert(double x, double y) {
			
			double summe = x + y;
			
			return summe;
		}

	public static void main(String[] args) {
		// TODO Auto-generated method stub


      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
		
	
		double x = 2.0;
		double y = 4.0;
		double m;
	      
	
	
	      
	    // (V) Verarbeitung
	    // Mittelwert von x und y berechnen: 
	    // ================================
	  
	      
	     m =  berechneMittelwert( x , y )/2.0;
	    
	    // (A) Ausgabe
	    // Ergebnis auf der Konsole ausgeben:
	    // =================================
	    System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	  }
		
}
		
		
	
