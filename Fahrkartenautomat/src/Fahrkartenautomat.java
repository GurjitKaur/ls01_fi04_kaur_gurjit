﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       //double Variablen
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       do {
    	   System.out.println("------------------");
    	   System.out.println("Neue Fahrkarte");
    	   System.out.println("------------------\n");
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
    	   warte(1000);
       
    	   // Geldeinwurf
    	   // -----------
    	   eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
    	   // Fahrscheinausgabe
    	   // -----------------
    	   fahrkartenAusgeben();
       
       

    	   // Rückgeldberechnung und -Ausgabe
    	   // -------------------------------
    	   rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);

    	   System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          	  "vor Fahrtantritt entwerten zu lassen!\n"+
                              "Wir wünschen Ihnen eine gute Fahrt.");
       } while (true);
    }
    public static double fahrkartenbestellungErfassen() {
    	boolean done = true;
    	double preisFuerEinTicket = 0;
    	do {
    	Scanner tastatur = new Scanner(System.in);
    	
    	
    	
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: ");
    	System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
    	System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
    	System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    	System.out.println("Bezahlen (9)");
    	System.out.printf("Geben Sie die Auswahl ein: ");
    	
    	
    	int benutzerwahl = tastatur.nextInt();
    	switch(benutzerwahl) {
    	case 1: preisFuerEinTicket += 2.9;
    			System.out.println("Sie kaufen jetzt eine Einzelfahrschein Regeltarif AB [2,90 EUR]\n");
    			break;
    	case 2: preisFuerEinTicket += 8.6;
    			System.out.println("Sie kaufen jetzt eine Tageskarte Regeltarif AB\n");
    			break;
    	case 3: preisFuerEinTicket += 23.5;
    			System.out.println("Sie kaufen jetzt eine Kleingruppen-Tageskarte Regeltarif AB\n");
    			break;
    	case 9: done = false;
    			break;
    	default: System.out.println("Falsche Auswahl, sie kaufen nun einen Einzelfahrschein\n");
    			preisFuerEinTicket += 2.9;
    			break;
    	} } while (done);
    	
    	
    	int TicketAnzahl;
    	double zuZahlenderBetrag = 0;
    	Scanner tastatur = new Scanner(System.in);
        System.out.printf("Anzahl der Tickets: ");
        TicketAnzahl = tastatur.nextInt();
        boolean NotValid = true;
        
        
        while (NotValid == true) {
         if(TicketAnzahl > 10) {
        	NotValid = true;
        	System.out.println("Zu hoher Ticketwert. Bitte eine Ticketanzahl von 1 bis 10 angeben");
        	 try {
           	   Thread.sleep(700);
              } catch (InterruptedException e) {
           	   e.printStackTrace();
    		 }
        	System.out.printf("Anzahl der Tickets: ");
            TicketAnzahl = tastatur.nextInt();
           
         }
         else if(TicketAnzahl < 1) {
        	NotValid = true;
        	System.out.println("Zu niedriger Ticketwert.  Bitte eine Ticketanzahl von 1 bis 10 angeben");
        	 try {
           	   Thread.sleep(700);
              } catch (InterruptedException e) {
           	   e.printStackTrace();
    		  }
        	System.out.printf("Anzahl der Tickets: ");
            TicketAnzahl = tastatur.nextInt();
           
          }
          else {
        	NotValid = false;
        	zuZahlenderBetrag = preisFuerEinTicket * TicketAnzahl;  
          }
         
        }
       return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	Scanner tastatur = new Scanner(System.in);
    	
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	  double Rest = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	  System.out.printf("Noch zu zahlen: " + "%.2f" + " EURO\n", Rest);
     	  System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	  eingeworfeneMünze = tastatur.nextDouble();
          eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
       return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) //int counter
        {
           System.out.print("=");
           try {
        	   Thread.sleep(250);
           } catch (InterruptedException e) {
 		// TODO Auto-generated catch block
        	   e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    	
    }
    
    public static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
    	double rückgabebetrag;
    	
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; //double
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
              muenzenAusgeben(2, "EURO");
 	          rückgabebetrag -= 2.0;
             }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
              muenzenAusgeben(1, "EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
              muenzenAusgeben(50, "CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
              muenzenAusgeben(20, "CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
              muenzenAusgeben(10, "CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag > 0.04)// 5 CENT-Münzen
            {
             muenzenAusgeben(5, "CENT");
             rückgabebetrag -= 0.05;
  	          
            }
        }
    }
   public static void warte(int millisekunden) {
    	
        try {
     	   Thread.sleep(millisekunden);
        } catch (InterruptedException e) {
		// TODO Auto-generated catch block
     	   e.printStackTrace();
        }
   }
        
   public static void muenzenAusgeben(int betrag, String einheit) {
       System.out.printf("%d %s\n", betrag, einheit);
        	
        
    }    
        
}